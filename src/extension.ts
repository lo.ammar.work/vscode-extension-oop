import * as vscode from 'vscode';

export function activate(context: vscode.ExtensionContext) {
	const commandBuilderClassGenerate = vscode.commands.registerCommand('vscode-extension-oop-typescript.commandBuilderClassGenerate', commandBuilderClassGenerateHandler);
	const commandBuilderClassGenerateWithBuilderSetId = vscode.commands.registerCommand('vscode-extension-oop-typescript.commandBuilderClassGenerateWithBuilderSetId', commandBuilderClassGenerateWithBuilderSetIdHandler);
	const commandGetterSetterClassGenerate = vscode.commands.registerCommand('vscode-extension-oop-typescript.commandGetterSetterClassGenerate', commandGetterSetterClassGenerateHandler);
	const commandAllArgsConstructorGenerate = vscode.commands.registerCommand('vscode-extension-oop-typescript.commandAllArgsConstructorGenerate', commandAllArgsConstructorGenerateHandler);
	const commandEqualsAndHashGenerate = vscode.commands.registerCommand('vscode-extension-oop-typescript.commandEqualsAndHashGenerate', commandEqualsAndHashGenerateHandler);
	context.subscriptions.push(commandBuilderClassGenerate);
	context.subscriptions.push(commandGetterSetterClassGenerate);
	context.subscriptions.push(commandAllArgsConstructorGenerate);
	context.subscriptions.push(commandEqualsAndHashGenerate);
}




function commandEqualsAndHashGenerateHandler() {
	const editor = vscode.window.activeTextEditor;
	if (!editor) {
		vscode.window.showInformationMessage('No editor is active');
		return;
	}
	const selection = editor.selection;
	if (!selection) {
		vscode.window.showInformationMessage('No selection property');
		return;
	}
	const documentTextAll = editor.document.getText();
	const documentTextArea = editor.document.getText(selection);
	const className = getClassName(documentTextAll);
	const classNameGeneric = findClassNameWithGeneric(documentTextAll);

	if (!className || !classNameGeneric) {
		vscode.window.showInformationMessage('No class found');
		return;
	}
	const lastClassEndPos = findClassEnd(documentTextAll, className);

	const { propertyDetails } = getPropertiesName(documentTextArea);
	if (propertyDetails.length === 0) {
		vscode.window.showInformationMessage('No property found you have to highlight property that you want to generate equal and hash');
		return;
	}
	const equalAndHashCode = generateEqualAndHashCode(propertyDetails, classNameGeneric);
	if (lastClassEndPos > 0) {
		const position = editor.document.positionAt(lastClassEndPos);
		editor.edit(editBuilder => {
			const newText = `\n${equalAndHashCode}`;
			editBuilder.insert(position, newText);
		}).then(success => {
			if (success) {
				console.log('class generated');
			} else {
				vscode.window.showErrorMessage('Failed to generate class');
			}
		});
	}
}

function generateEqualAndHashCode(propertyDetails: IPropertyDetail[], classNameGeneric: string) {

	let property = '';
	propertyDetails.forEach(({ name }, index) => {
		property += `this.${name} === o.${name} ${index !== propertyDetails.length - 1 ? '&& ' : ''}`;
	});
	let propertyHash = '';
	propertyDetails.forEach(({ name }, index) => {
		propertyHash += `this.${name}${index !== propertyDetails.length - 1 ? ',' : ''}`;
	});
	return `\n    public equals(o:${classNameGeneric}): boolean {\n        if(this === o) return true;\n        if(!o) return false;\n        return ${property};\n    }\n\n    public hashCode(): number {\n        return Objects.hash(${propertyHash});\n    }\n`;
}

function findClassNameWithGeneric(documentText: string) {
	const regexClassName = /\bclass\s+(\w+)(<.*?>)?/g;
	let matchClass;
	const classNames = [];

	while ((matchClass = regexClassName.exec(documentText)) !== null) {
		// Combine the class name and its generic type if present
		const fullClassName = matchClass[1] + (matchClass[2] || '');
		classNames.push(fullClassName);
	}
	return classNames.length > 0 ? classNames[0] : null;
}

function getClassName(documentText: string) {
	const regexClassName = /class\s+(\w+)/;
	const matchClass = documentText.match(regexClassName);
	const className = matchClass ? matchClass[1] : null;
	return className;
}

function commandAllArgsConstructorGenerateHandler() {
	const editor = vscode.window.activeTextEditor;
	if (!editor) {
		vscode.window.showInformationMessage('No editor is active');
		return;
	}
	const documentText = editor.document.getText();
	const className = getClassName(documentText);
	if (!className) {
		vscode.window.showInformationMessage('No class found');
		return;
	}
	const { propertyDetails, lastClassEndPos } = getPropertiesName(documentText);
	const allArgConstructor = generateAllArgsConstructor(propertyDetails);
	if (lastClassEndPos > 0) {
		const position = editor.document.positionAt(lastClassEndPos);
		editor.edit(editBuilder => {
			const newText = `\n\n${allArgConstructor}`;
			editBuilder.insert(position, newText);
		}).then(success => {
			if (success) {
				console.log('Builder class generated');
			} else {
				vscode.window.showErrorMessage('Failed to generate builder class');
			}
		});
	}
}


function commandBuilderClassGenerateHandler() {
	const editor = vscode.window.activeTextEditor;
	if (!editor) {
		vscode.window.showInformationMessage('No editor is active');
		return;
	}
	const documentText = editor.document.getText();
	const className = getClassName(documentText);
	const classNameGeneric = findClassNameWithGeneric(documentText);
	if (!classNameGeneric) {
		vscode.window.showInformationMessage('No class found');
		return;
	}

	if (!className) {
		vscode.window.showInformationMessage('No class found');
		return;
	}
	const generic = extractGenericFromClassName(classNameGeneric);
	const { propertyDetails, lastClassEndPos } = getPropertiesName(documentText);
	const builderMethod = generateBuilderMethod(className, generic);
	const getterSetterMethod = generateGetterAndSetterMethod(propertyDetails);
	const builderClass = generateBuilderClass(className, generic, propertyDetails);

	if (lastClassEndPos > 0) {
		const position = editor.document.positionAt(lastClassEndPos);
		editor.edit(editBuilder => {
			removeCloseBracketFirstClass(editor, editBuilder, documentText);
			const position = editor.document.positionAt(lastClassEndPos);
			const newText = `\n\n${builderMethod}\n\n${getterSetterMethod}\n\n}\n\n${builderClass}\n`;
			editBuilder.insert(position, newText);
		}).then(success => {
			if (success) {
				console.log('Builder class generated');
			} else {
				vscode.window.showErrorMessage('Failed to generate builder class');
			}
		});
	}
}

function commandBuilderClassGenerateWithBuilderSetIdHandler() {
	const editor = vscode.window.activeTextEditor;
	if (!editor) {
		vscode.window.showInformationMessage('No editor is active');
		return;
	}
	const documentText = editor.document.getText();
	const className = getClassName(documentText);
	const classNameGeneric = findClassNameWithGeneric(documentText);
	if (!classNameGeneric) {
		vscode.window.showInformationMessage('No class found');
		return;
	}

	if (!className) {
		vscode.window.showInformationMessage('No class found');
		return;
	}
	const generic = extractGenericFromClassName(classNameGeneric);
	const { propertyDetails, lastClassEndPos } = getPropertiesName(documentText);
	const builderMethod = generateBuilderMethod(className, generic);
	const getterSetterMethod = generateGetterAndSetterMethod(propertyDetails);
	const builderClass = generateBuilderClassWithBuilderSetId(className, generic, propertyDetails);

	if (lastClassEndPos > 0) {
		const position = editor.document.positionAt(lastClassEndPos);
		editor.edit(editBuilder => {
			removeCloseBracketFirstClass(editor, editBuilder, documentText);
			const position = editor.document.positionAt(lastClassEndPos);
			const newText = `\n\n${builderMethod}\n\n${getterSetterMethod}\n\n}\n\n${builderClass}\n`;
			editBuilder.insert(position, newText);
		}).then(success => {
			if (success) {
				console.log('Builder class generated');
			} else {
				vscode.window.showErrorMessage('Failed to generate builder class');
			}
		});
	}
}


function extractGenericFromClassName(classNameGeneric: string) {
	const pattern = /<([^>]+)>/g;
	const extractedValues = [];

	// Use a while loop with exec to find all matches in the input string
	let match;
	while ((match = pattern.exec(classNameGeneric)) !== null) {
		extractedValues.push(match[1]); // Capture group [1] contains the value without angle brackets
	}
	return extractedValues.length > 0 ? extractedValues[0] : null;

}

function commandGetterSetterClassGenerateHandler() {
	const editor = vscode.window.activeTextEditor;
	if (!editor) {
		vscode.window.showInformationMessage('No editor is active');
		return;
	}
	const documentText = editor.document.getText();
	const className = getClassName(documentText);
	if (!className) {
		vscode.window.showInformationMessage('No class found');
		return;
	}
	const { propertyDetails, lastClassEndPos } = getPropertiesName(documentText);
	const getterSetterMethod = generateGetterAndSetterMethod(propertyDetails);


	if (lastClassEndPos > 0) {
		const position = editor.document.positionAt(lastClassEndPos);
		editor.edit(editBuilder => {

			const position = editor.document.positionAt(lastClassEndPos);
			const newText = `\n\n${getterSetterMethod}`;
			editBuilder.insert(position, newText);
		}).then(success => {
			if (success) {
				console.log('Generated');
			} else {
				vscode.window.showErrorMessage('Failed to generate');
			}
		});
	}
}

interface IPropertyDetail {
	access: string;
	name: string;
	type: string;
}

function generateAllArgsConstructor(propertyDetails: IPropertyDetail[]) {
	let argument = '';
	propertyDetails.forEach(({ name, type }, index) => {
		argument += `\n        ${name}:${type}${index === propertyDetails.length - 1 ? "" : ","}`;
	});
	let argumentAssign = '';
	propertyDetails.forEach(({ name }) => {
		argumentAssign += `\n        this.${name} = ${name}`;
	});
	return `\n    constructor(${argument}\n    ) {${argumentAssign}\n    }`;
}

function capitalize(str: string) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}



function getPropertiesName(documentText: string) {
	const regexProperty = /\b(private|protected)\s+(\w+)\s*:\s*([^;]+);/g;
	let match: RegExpExecArray | null = null;
	let lastClassEndPos = 0;
	const propertyDetails: IPropertyDetail[] = [];

	while ((match = regexProperty.exec(documentText)) !== null) {
		propertyDetails.push({ access: match[1], name: match[2], type: match[3] });
		lastClassEndPos = match.index + match[0].length;
	}
	return { propertyDetails, lastClassEndPos };
}

function generateBuilderMethod(className: string, generic: string | null) {
	const genericRender = generic ? `<${generic}>` : '';
	return className ? `    static builder${genericRender}(): ${className}Builder${genericRender} {\n        return new ${className}Builder${genericRender}();\n    }` : '';
}

function generateGetterAndSetterMethod(propertyDetails: IPropertyDetail[]) {
	const gettersAndSetters = propertyDetails.map(({ name, type, access }) => {
		const getterPrefix = type === 'boolean' ? 'is' : 'get';
		const getterSetterAccess = access === 'protected' ? 'public' : 'public';
		return `    ${getterSetterAccess} ${getterPrefix}${capitalize(name)}(): ${type} {\n        return this.${name};\n    }\n\n    ${getterSetterAccess} set${capitalize(name)}(value: ${type}): void {\n        this.${name} = value;\n    }`;
	}).join('\n\n');
	return gettersAndSetters;
}

function generateBuilderClass(className: string, generic: string | null, propertyDetails: IPropertyDetail[]) {
	const genericRender = generic ? `<${generic}>` : '';
	const builderClassName = `${className}Builder`;
	let classCode = `class ${builderClassName}${genericRender} {\n    private ${toCamelCase(className)}:${capitalize(className)}${genericRender};\n\n    constructor(){\n        this.${toCamelCase(className)} = new ${className}${genericRender}();\n    }`;

	classCode += '\n';

	propertyDetails.forEach(({ name, type }) => {
		classCode += `    public set${capitalize(name)}(${name}: ${type}): ${builderClassName}${genericRender} {\n        this.${toCamelCase(className)}.set${capitalize(name)}(${name});\n        return this;\n    }\n\n`;
	});
	let builderCondition = '\n';
	propertyDetails.forEach(({ name, type }) => {
		builderCondition += generateConditionBuilder(className, name, type);
	});

	classCode += `    public build(): ${className}${genericRender} {\n${builderCondition}\n        return this.${toCamelCase(className)}\n    }\n}`;

	return classCode;
}

function generateBuilderClassWithBuilderSetId(className: string, generic: string | null, propertyDetails: IPropertyDetail[]) {
	const genericRender = generic ? `<${generic}>` : '';
	const builderClassName = `${className}Builder`;
	let classCode = `class ${builderClassName}${genericRender} {\n    private ${toCamelCase(className)}:${capitalize(className)}${genericRender};\n\n    constructor(){\n        this.${toCamelCase(className)} = new ${className}${genericRender}();\n    }`;

	classCode += '\n';
	classCode += `    public set${capitalize('id')}(${toCamelCase(className) + "Id"}: ${className + "Id"}): ${builderClassName}${genericRender} {\n        this.${toCamelCase(className)}.set${capitalize('id')}(${toCamelCase(className) + "Id"});\n        return this;\n    }\n\n`
	propertyDetails.forEach(({ name, type }) => {
		classCode += `    public set${capitalize(name)}(${name}: ${type}): ${builderClassName}${genericRender} {\n        this.${toCamelCase(className)}.set${capitalize(name)}(${name});\n        return this;\n    }\n\n`;
	});
	let builderCondition = '\n';
	builderCondition += generateConditionBuilderWithBuilderSetId(className, 'id', '');
	propertyDetails.forEach(({ name, type }) => {
		builderCondition += generateConditionBuilder(className, name, type);
	});

	classCode += `    public build(): ${className}${genericRender} {\n${builderCondition}\n        return this.${toCamelCase(className)}\n    }\n}`;

	return classCode;
}

function generateConditionBuilder(className: string, propertyName: string, type: string) {
	return `        if (!this.${toCamelCase(className)}.${type === 'boolean' ? 'is' : 'get'}${capitalize(propertyName)}()) {\n            throw new Error('${propertyName} is required!');\n        }\n`;
}
function generateConditionBuilderWithBuilderSetId(className: string, propertyName: string, type: string) {
	return `        if (!this.${toCamelCase(className)}.${type === 'boolean' ? 'is' : 'get'}${capitalize(propertyName)}().getValue()) {\n            throw new Error('${toCamelCase(className) + " id"} is required!');\n        }\n`;
}


function removeCloseBracketFirstClass(editor: vscode.TextEditor, editBuilder: vscode.TextEditorEdit, documentText: string) {
	const regexEndOfClass = /\}/g; // Regular expression to find closing brace
	let lastMatch: RegExpExecArray | null = null;;
	let match: RegExpExecArray | null;
	while ((match = regexEndOfClass.exec(documentText)) !== null) {
		lastMatch = match;
	}
	if (lastMatch) {
		const bracePosition = editor.document.positionAt(lastMatch.index);
		editBuilder.delete(new vscode.Range(bracePosition, bracePosition.translate(0, 1)));
	} else {
		console.error("No closing brace found in the class.");
	}
}

function findClassEnd(documentText: string, className: string) {
	let classStartIndex = documentText.indexOf(`class ${className}`);
	if (classStartIndex === -1) {
		return -1;
	}

	classStartIndex = documentText.indexOf('{', classStartIndex);
	if (classStartIndex === -1) {
		return -1;
	}

	let braceCount = 1;
	for (let i = classStartIndex + 1; i < documentText.length; i++) {
		if (documentText[i] === '{') {
			braceCount++;
		} else if (documentText[i] === '}') {
			braceCount--;
			if (braceCount === 0) {
				return i;
			}
		}
	}

	return -1;
}

function toCamelCase(text: string) {
	if (!text) {
		return text;
	}
	return text.charAt(0).toLowerCase() + text.slice(1);
}

export function deactivate() { }
