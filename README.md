# VSCode Extension OOP Typescript README

This Visual Studio Code extension streamlines the rapid generation of TypeScript classes by offering key features, including:

## Features

- **Builder**: Automatically generate a fluent builder for TypeScript classes.
- **AllArgsConstructor**: Create constructors with all class fields as parameters.
- **Getter/Setter**: Efficiently generate getter and setter methods for class fields.
- **Equal/HashCode**: Generate equality checks and a hash code for class properties.

## Requirements

To make use of this extension, ensure that you have the following prerequisites installed:

- Visual Studio Code version ^1.84.0
- TypeScript version 4.x or later

## How to Use

To effectively utilize this extension, follow these steps:

1. Open your TypeScript file in Visual Studio Code.
2. Create a class within your document, as demonstrated in the following example:

   ```typescript
   class Test {
       private property: string;
   }

1. Execute the desired command by pressing Ctrl + Shift + P and typing one of the following options:

- "Builder Class Generate"
- "Builder Class Generate With Builder Id"
- "AllArgsConstructor Generate"
- "Getter Setter Class Generate"
- "Equals and Hash Generate"

2. To create equality checks and generate a hash for a property, highlight the property you intend to apply these operations to. Please be aware that this command does not support arrays or objects.

3. If you want to use Equals and Hash Generate you have to make this helper function hash to you directory in .ts file

    ```typescript
    export class Objects {
        static hashInitialValue = 17;
        private static hashSingle(obj: any) {
            if (obj === null || obj === undefined) {
                return 0;
            }

            const hashString: string = (typeof obj === 'object') ? JSON.stringify(obj) : obj.toString();
            let hash = 0;

            for (let i = 0; i < hashString.length; i++) {
                const char = hashString.charCodeAt(i);
                hash = ((hash << 5) - hash) + char;
                hash |= 0;
            }
            return hash;
        }

        static hash(...args: any[]) {
            let finalHash = Objects.hashInitialValue;

            for (const arg of args) {
                const hash = Objects.hashSingle(arg);
                finalHash = ((finalHash << 5) - finalHash) + hash;
                finalHash |= 0;
            }

            return finalHash;
        }
    }

## Release Notes

0.0.1

- Initial release: Includes fundamental features for class generation.

0.0.6

- Make builder support generic types

0.0.7

- Change hashCode to helper for clean code and support object, string, number

0.1.0

- Add builder function with builder set id and check